# SearchProduct

## Basic Features

This is a Kotlin Spring Boot application where it allows the user to enter a searchTerm to search for products 
in Search and Product APIs.

Please see the comments in the src code for further details.

Check the error handling through the implementation of Exception Handler.

## Requirements:
Create a service in Kotlin that can
1. issue a search and return the searchapi response combined with all the appropriate
   details from the productapi for each result;
2. add a price range field to each product that provides a formatted representation of the
   min and max prices available in that product.

## Assumptions
1. There is an assumption that the Search API and Product API exist, that would return such responses to be processed by the API that I will develop.
2. I used mocky.io to mock the responses of the Search API and Product API.
3. When the searchTerm param is "error", it will return an empty list of products.

## Getting started

1. Checkout the project
2. Make sure all the dependencies are downloaded
3. Start running the application


## Sample CURL for Testing
```
curl --location 'http://localhost:1052/application-demo/product?searchTerm=modern' \
```

where the fileName is the name of the .csv file.

## Sample Response:
```
{
    "meta": {
        "searchTerm": "modern",
        "count": 3
    },
    "items": [
        {
            "id": 111,
            "name": "Modern Chair",
            "priceRange": "123.0 - 154.0",
            "options": [
                {
                    "id": 4,
                    "name": "Blue",
                    "price": 123.0
                },
                {
                    "id": 5,
                    "name": "Red",
                    "price": 154.0
                }
            ]
        },
        {
            "id": 222,
            "name": "Modern Table",
            "priceRange": "1223.0 - 2154.0",
            "options": [
                {
                    "id": 6,
                    "name": "Wood",
                    "price": 1223.0
                },
                {
                    "id": 7,
                    "name": "Metal",
                    "price": 2154.0
                }
            ]
        },
        {
            "id": 333,
            "name": "Modern Couch",
            "priceRange": "892.0 - 1054.0",
            "options": [
                {
                    "id": 8,
                    "name": "Fuzzy",
                    "price": 892.0
                },
                {
                    "id": 9,
                    "name": "Leather",
                    "price": 1054.0
                }
            ]
        }
    ]
}
```
