import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "3.2.1"
	id("io.spring.dependency-management") version "1.1.4"
	kotlin("jvm") version "1.9.21"
	kotlin("plugin.spring") version "1.9.21"
	kotlin("plugin.jpa") version "1.9.21"
}

group = "com.example"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_17
}

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
}

extra["springCloudVersion"] = "2023.0.0"

dependencies {

	// CORE
	implementation ("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation ("org.jetbrains.kotlin:kotlin-reflect")
	implementation ("org.springframework.boot:spring-boot-starter-parent:2.7.12")
	implementation ("org.springframework.boot:spring-boot-starter-web")
	implementation ("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.springframework.boot:spring-boot-starter-validation")
	implementation ("org.springframework.boot:spring-boot-starter-webflux")

	// SECURITY
	implementation ("org.springframework.security:spring-security-web")
	implementation ("org.springframework.boot:spring-boot-starter-security")

	// OAUTH Client
	implementation ("org.springframework.security:spring-security-oauth2-client:5.7.5")

	// DOC
	implementation ("org.springdoc:springdoc-openapi-ui:1.6.9")

	// MONITORING
	implementation ("org.springframework.boot:spring-boot-starter-actuator")
	implementation ("io.micrometer:micrometer-registry-prometheus:1.9.8")

	// LOGS
	implementation ("org.aspectj:aspectjweaver:1.9.9.1")
	implementation ("net.logstash.logback:logstash-logback-encoder:7.0.1")
	implementation ("com.jayway.jsonpath:json-path:2.7.0") // for the PIIMasker usage

	// DB
//	implementation ("org.postgresql:postgresql:42.4.1")
//	implementation ("org.mybatis.spring.boot:mybatis-spring-boot-starter:2.1.4")

	// TEST
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
	testImplementation("org.spockframework:spock-core:1.3-groovy-2.5")
	testImplementation("org.spockframework:spock-spring:1.3-groovy-2.5")
	testImplementation("org.mockito.kotlin:mockito-kotlin:3.2.0")
	testImplementation("com.ninja-squad:springmockk:3.1.1")
}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs += "-Xjsr305=strict"
		jvmTarget = "17"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
