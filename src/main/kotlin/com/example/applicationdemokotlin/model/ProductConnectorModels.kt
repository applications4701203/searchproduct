package com.example.applicationdemokotlin.model

data class ProductConnectorResponse(
        val id: Int,
        val name: String,
        val options: List<OptionItem>
)