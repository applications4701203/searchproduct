package com.example.applicationdemokotlin.model

data class SearchConnectorResponse(
        val searchTerm: String,
        val itemIds: List<Int>
)
