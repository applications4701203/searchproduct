package com.example.applicationdemokotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(basePackages = ["com.example.applicationdemokotlin", "com.example.applicationdemokotlin.config"])
class ApplicationdemokotlinApplication

fun main(args: Array<String>) {
	runApplication<ApplicationdemokotlinApplication>(*args)
}
