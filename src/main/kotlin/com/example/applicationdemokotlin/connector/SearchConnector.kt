package com.example.applicationdemokotlin.connector

import com.example.applicationdemokotlin.model.SearchConnectorResponse
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.stereotype.Component
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

@Component
class SearchConnector(
        private val mapper: ObjectMapper,
        restTemplateBuilder: RestTemplateBuilder
) {

    private val restTemplate: RestTemplate = restTemplateBuilder
            .messageConverters(MappingJackson2HttpMessageConverter().apply { objectMapper = mapper })
            .build()

    fun searchItem(searchTerm: String): SearchConnectorResponse? {

        return try {
            val respEntity: ResponseEntity<SearchConnectorResponse> =
                    restTemplate.exchange(
                            mockUrls(searchTerm),
                            HttpMethod.GET,
                    )

            respEntity.body!!
        } catch (e: HttpClientErrorException) {
            null
        } catch (e: HttpServerErrorException) {
            null
        }
    }

    // For Test Purposes Only
    private fun mockUrls(searchTerm: String): String {
        return when(searchTerm) {
            "error" -> "https://run.mocky.io/v3/58661a42-65ae-4c1d-85e0-581aa58f772b" // returns empty string
            else -> "https://run.mocky.io/v3/64d46c93-d931-4411-a0db-ac9111939664"
        }
    }

}