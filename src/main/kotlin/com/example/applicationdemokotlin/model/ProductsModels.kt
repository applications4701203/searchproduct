package com.example.applicationdemokotlin.model

import java.math.BigDecimal

data class Products(
    val meta: Meta,
    val items: List<Item>
)

data class Meta (
    val searchTerm: String,
    val count: Int
)

data class Item (
    val id: Int,
    val name: String,
    val priceRange: String,
    val options: List<OptionItem>
)

data class OptionItem (
    val id: Int,
    val name: String,
    val price: BigDecimal
)