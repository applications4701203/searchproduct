package com.example.applicationdemokotlin.service

import com.example.applicationdemokotlin.connector.ProductConnector
import com.example.applicationdemokotlin.connector.SearchConnector
import com.example.applicationdemokotlin.model.Item
import com.example.applicationdemokotlin.model.Meta
import com.example.applicationdemokotlin.model.Products
import org.springframework.stereotype.Service

interface ProductsService {

    fun searchProducts(searchTerm: String): Products
}

@Service
class ProductsServiceImpl(
        private val productConnector: ProductConnector,
        private val searchConnector: SearchConnector
): ProductsService {

    override fun searchProducts(searchTerm: String): Products {

        // API call to search the Product IDs from the searchTerm
        val searchProductResponse = searchConnector.searchItem(searchTerm)

        val items: List<Item> = searchProductResponse?.itemIds!!.map { item ->

            // API call to search the Product Details based from the returned Product IDs of Search API
            val productConnectorResponse = productConnector.searchProduct(item)!!

            // Getting the minimum and maximum price among the option items
            val minPriceRange: String = productConnectorResponse.options.minBy { it.price }.price.toString()
            val maxPriceRange: String = productConnectorResponse.options.maxBy { it.price }.price.toString()

            // adding onto the list of items the response from Product Details API
            Item(productConnectorResponse.id, productConnectorResponse.name,
                    "$minPriceRange - $maxPriceRange", productConnectorResponse.options)
        }
        return Products(Meta(searchTerm, items.size), items)
    }
}
