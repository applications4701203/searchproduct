package com.example.applicationdemokotlin.controller

import com.example.applicationdemokotlin.exception.EmptySearchException
import com.example.applicationdemokotlin.model.Products
import com.example.applicationdemokotlin.service.ProductsService
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.validation.ConstraintViolationException
import jakarta.validation.constraints.Size
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.HttpClientErrorException

@Tag(name = "Products API - Products Controller")
@RestController
@RequestMapping("/product")
@Validated
class ProductsController(
        private val productsService: ProductsService
) {

    /**
     * API endpoint for product search.
     *
     * @throws EmptySearchException if searchTerm param is not present
     * @throws ConstraintViolationException if searchTerm is empty or exceeds 30 characters
     * @throws HttpClientErrorException and HttpServerErrorException if external API has server issues.
     *
     * @return Products containing the product details and number of products.
     */
    @GetMapping
    fun searchProducts(@RequestParam("searchTerm") @Size(max=30) searchTerm: String): Products {
        if (searchTerm == "") throw EmptySearchException()
        return productsService.searchProducts(searchTerm);
    }
}