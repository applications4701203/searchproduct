package com.example.applicationdemokotlin.exception

enum class ErrorCode {
    BAD_REQUEST, UNPROCESSABLE
}

open class DefaultError(
        val status: ErrorCode,
        val code: String,
        val reason: String
): Exception(reason)

data class DefaultErrorResponse(
        val status: ErrorCode,
        val code: String,
        val reason: String
)

class EmptySearchException : DefaultError
    (ErrorCode.BAD_REQUEST, "ESX001", "SEARCH_STRING_EMPTY")


