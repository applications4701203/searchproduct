package com.example.applicationdemokotlin.connector

import com.example.applicationdemokotlin.model.ProductConnectorResponse
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.stereotype.Component
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

@Component
class ProductConnector(
        private val mapper: ObjectMapper,
        restTemplateBuilder: RestTemplateBuilder
) {

    private val restTemplate: RestTemplate = restTemplateBuilder
            .messageConverters(MappingJackson2HttpMessageConverter().apply { objectMapper = mapper })
            .build()

    fun searchProduct(itemId: Int): ProductConnectorResponse? {

        return try {
            val respEntity: ResponseEntity<ProductConnectorResponse> =
                    restTemplate.exchange(
                            mockUrls(itemId),
                            HttpMethod.GET,
                    )

            respEntity.body
        } catch (e: HttpClientErrorException) {
            null
        } catch (e: HttpServerErrorException) {
            null
        }
    }

    // For Test Purposes Only
    private fun mockUrls(itemId: Int): String {
        return when(itemId) {
            111 -> "https://run.mocky.io/v3/00eac614-ca71-4b78-aeef-7fc461d4130d"
            222 -> "https://run.mocky.io/v3/f7aa2206-49ec-4e02-9c3c-ccf8c77c0a9e"
            333 -> "https://run.mocky.io/v3/07d2a2eb-f9b5-4327-8e21-58f797043813"
            else -> ""
        }
    }

}