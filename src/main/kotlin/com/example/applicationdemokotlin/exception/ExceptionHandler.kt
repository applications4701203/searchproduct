package com.example.applicationdemokotlin.exception

import jakarta.validation.ConstraintViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(EmptySearchException::class)
    fun emptySearchExceptionHandler(ex: EmptySearchException): ResponseEntity<Any> {
        return ResponseEntity(DefaultErrorResponse(ex.status, ex.code, ex.reason), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(ConstraintViolationException::class)
    fun constraintViolationExceptionHandler(ex: ConstraintViolationException): ResponseEntity<Any> {
        return ResponseEntity(DefaultErrorResponse(ErrorCode.BAD_REQUEST, "CVE001", "REQUIRED_REQUEST_PARAM MISSING"), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(HttpClientErrorException::class)
    fun httpClientErrorExceptionHandler(ex: HttpClientErrorException): ResponseEntity<Any> {
        return ResponseEntity(DefaultErrorResponse(ErrorCode.UNPROCESSABLE, "HTTP001", "HTTP_CLIENT_CALL_UNSUCCESSFUL"), HttpStatus.UNPROCESSABLE_ENTITY)
    }

    @ExceptionHandler(HttpServerErrorException::class)
    fun httpServerErrorExceptionHandler(ex: HttpServerErrorException): ResponseEntity<Any> {
        return ResponseEntity(DefaultErrorResponse(ErrorCode.UNPROCESSABLE, "HTTP002", "HTTP_SERVER_UNAVAILABLE"), HttpStatus.UNPROCESSABLE_ENTITY)
    }
}